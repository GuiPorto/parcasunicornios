import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import {NotifierModule} from 'angular-notifier';

import {ModulesUtils} from '../utils/modules.utils';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { UnicornComponent } from './unicorn/unicorn.component';
import { BroComponent } from './bro/bro.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { RegisterComponent } from './register/register.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

const utils = ModulesUtils;

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    UnicornComponent,
    BroComponent,
    PortfolioComponent,
    RegisterComponent,
    NavBarComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,

    NotifierModule.withConfig(utils.notifyOptions()),

    AppRoutingModule,
    HttpClientModule,
    DragDropModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
