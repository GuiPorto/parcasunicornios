import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {User} from '../../model/user.model';
import {UserService} from '../../services/user.service';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  private readonly notifier: NotifierService;
  form: FormGroup;

  user: User = new User();

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private notifierService: NotifierService,
              private userService: UserService) {
    this.notifier = notifierService;
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: [null],
      date: [null],
      document_RG: [null],
      document_CPF: [null],
      nickname: [null],
      email: [null],
    });
  }

  // tslint:disable-next-line:typedef
  submit() {
    this.user = this.form.value;
    this.userService.postNew(this.user).subscribe(
      () => {
        this.router.navigate(['']);
      },
      () => {
        console.log('Não foi possível cadastrar');
      }
    );
  }

}
