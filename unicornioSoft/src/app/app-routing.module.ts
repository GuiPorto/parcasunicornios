import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import {LoginComponent} from './login/login.component';
import {BroComponent} from './bro/bro.component';
import {PortfolioComponent} from './portfolio/portfolio.component';
import {RegisterComponent} from './register/register.component';
import {UnicornComponent} from './unicorn/unicorn.component';


const routes: Routes = [
  {path: 'bro', component: BroComponent},
  {path: 'home', component: HomeComponent},
  {path: 'port', component: PortfolioComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'unicorn', component: UnicornComponent},
  {path: '', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
