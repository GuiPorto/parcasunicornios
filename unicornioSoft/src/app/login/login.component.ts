import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {User} from '../../model/user.model';
import {SessionService} from '../../services/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  user: User = new User();
  form: FormGroup;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private sessionService: SessionService) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      id: [null],
      password: [null],
    });
  }


  // tslint:disable-next-line:typedef
  submit() {
    this.user = this.form.value;
    this.sessionService.postSession(this.user).subscribe(
      () => {
        this.router.navigate(['/home']);
      },
      () => {
        console.log('não encontrado');
      }
    );
  }

}
