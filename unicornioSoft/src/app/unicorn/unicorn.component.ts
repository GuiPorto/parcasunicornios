import {Component, OnInit} from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Unicorn} from '../../model/unicorn.model';
import {UnicornService} from '../../services/unicorn.service';

@Component({
  selector: 'app-unicorn',
  templateUrl: './unicorn.component.html',
  styleUrls: ['./unicorn.component.scss']
})
export class UnicornComponent implements OnInit {
  show = false;
  form: FormGroup;
  uni1: Array<Unicorn> = [];
  uni2: Array<Unicorn> = [];
  uni3: Array<Unicorn> = [];
  uni4: Array<Unicorn> = [];

  constructor(private formBuilder: FormBuilder,
              private unicornService: UnicornService) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      id: [null],
      type: [null],
      description: [null],
    });
    this.unicornService.getUni().subscribe(
      data => {
        data.forEach(item => {
          if (item.type === '1') {
            this.uni1.push(item);
          } else if (item.type === '2') {
            this.uni2.push(item);
          } else if (item.type === '3') {
            this.uni3.push(item);
          } else {
            this.uni4.push(item);
          }
        });
      }
    );
  }


  t1() {
    this.form.reset();
    this.show = true;
    this.form.controls.type.setValue(1);
  }


  t2() {
    this.form.reset();
    this.show = true;
    this.form.controls.type.setValue(2);
  }


  t3() {
    this.form.reset();
    this.show = true;
    this.form.controls.type.setValue(3);
  }


  t4() {
    this.form.reset();
    this.show = true;
    this.form.controls.type.setValue(4);
  }


  add() {
    if (this.form.controls.type.value === 1) {
      this.uni1.push(this.form.value);
      const aux = this.form.value;
      this.unicornService.postUni(aux).subscribe(
        () => {
          console.log('success');
        }, () => {
          console.log('error');
        }
      )
    } else if (this.form.controls.type.value === 2) {
      this.uni2.push(this.form.value);
      const aux = this.form.value;
      this.unicornService.postUni(aux).subscribe(
        () => {
          console.log('success');
        }, () => {
          console.log('error');
        }
      )
    } else if (this.form.controls.type.value === 3) {
      this.uni3.push(this.form.value);
      const aux = this.form.value;
      this.unicornService.postUni(aux).subscribe(
        () => {
          console.log('success');
        }, () => {
          console.log('error');
        }
      )
    } else {
      this.uni4.push(this.form.value);
      const aux = this.form.value;
      this.unicornService.postUni(aux).subscribe(
        () => {
          console.log('success');
        }, () => {
          console.log('error');
        }
      )
    }
    this.show = false;
  }

  drop(event: CdkDragDrop<Unicorn[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  delete(id) {
    if (this.form.controls.type.value === 1) {
this.unicornService.delete(id).subscribe(
  () => {
    console.log('delete success');
  }, () => {
    console.log('bad request');
  }
)
    } else  if (this.form.controls.type.value === 2) {
      this.unicornService.delete(id).subscribe(
        () => {
          console.log('delete success');
        }, () => {
          console.log('bad request');
        }
      )
    } else  if (this.form.controls.type.value === 3) {
      this.unicornService.delete(id).subscribe(
        () => {
          console.log('delete success');
        }, () => {
          console.log('bad request');
        }
      )
    } else {
      this.unicornService.delete(id).subscribe(
        () => {
          console.log('delete success');
        }, () => {
          console.log('bad request');
        }
      )
    }
  }

}
