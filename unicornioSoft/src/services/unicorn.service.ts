import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../environments/environment';
import {Observable} from 'rxjs';

import {Unicorn} from '../model/unicorn.model';

@Injectable({
  providedIn: 'root'
})
export class UnicornService {

  headers: any;

  constructor(private http: HttpClient) {
  }

  postUni(unicorn: Unicorn): Observable<any> {
    this.headers = {Authorization: JSON.parse( localStorage.getItem('user'))};
    return this.http.post(`${environment.apiUrl}/unicorn`, unicorn, {headers: this.headers});
  }
  getUni(): Observable<any> {
    this.headers = {Authorization: JSON.parse( localStorage.getItem('user'))};
    return this.http.get(`${environment.apiUrl}/unicorn`, {headers: this.headers});
  }

  delete(id: number): Observable<any> {
    this.headers = {Authorization: JSON.parse( localStorage.getItem('user'))};
    return this.http.delete(`${environment.apiUrl}/unicorn/${id}`, {headers: this.headers});
  }

}
