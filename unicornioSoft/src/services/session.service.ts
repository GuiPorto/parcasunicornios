import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../environments/environment';
import {Observable} from 'rxjs';

import {User} from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  headers: any;

  constructor(private http: HttpClient) {
  }

  postSession(session: User): Observable<any> {
    localStorage.setItem('user', JSON.stringify(session.id));
    return this.http.post(`${environment.apiUrl}`, session);
  }

}
