import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../environments/environment';
import {Observable} from 'rxjs';

import {User} from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  headers: any;

  constructor(private http: HttpClient) {
  }

  postNew(newUser: User): Observable<any> {
    return this.http.post(`${environment.apiUrl}/register`, newUser);
  }
  getUser(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/bro`, {headers: this.headers});
  }

}

