export class Unicorn {
  id: number;
  type: string;
  description: string;
}
