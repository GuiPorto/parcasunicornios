/* tslint:disable:variable-name */
export class User {
  id: string;
  name: string;
  date: string;
  document_RG: string ;
  document_CPF: string;
  nickname: string;
  email: string;
  password: string;
}
