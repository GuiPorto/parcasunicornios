import {NotifierOptions} from 'angular-notifier';

export class ModulesUtils {

  static notifyOptions(): NotifierOptions {
    const customNotifierOptions: NotifierOptions = {
      position: {
        horizontal: {
          position: 'middle',
        },
        vertical: {
          position: 'top',
        }
      },
      behaviour: {
        autoHide: 3000,
        onClick: 'hide',
        onMouseover: 'pauseAutoHide',
        showDismissButton: false,
        stacking: 0
      },
      animations: {
        enabled: true,
        show: {
          preset: 'fade',
        },
        hide: {
          preset: 'fade',
        }
      }
    };
    return customNotifierOptions;
  }
}



