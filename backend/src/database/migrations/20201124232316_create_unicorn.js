
exports.up = function(knex) {
    return knex.schema.createTable('unicorn', function (table) {
        table.increments();
        table.string('type').notNullable();
        table.string('description').notNullable();

        table.string('user_id').notNullable();

        table.foreign('user_id').references('id').inTable('user');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('unicorn');
};
