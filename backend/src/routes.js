const express = require('express');

const UserController = require('./controllers/UserController');
const UnicornController = require('./controllers/UnicornController');
const ProfileController = require('./controllers/ProfileController');
const SessionController = require('./controllers/SessionController');

const routes = express.Router();

routes.post('', SessionController.store);

routes.get('/register', UserController.findAll);
routes.get('/bro', UserController.findOne);
routes.post('/register', UserController.store);

routes.get('/unicorn', ProfileController.findOne);

routes.get('/unicornAll', UnicornController.findAll);
routes.post('/unicorn', UnicornController.store);
routes.delete('/unicorn/:id', UnicornController.delete);




module.exports = routes;
