const connection = require('../database/connection');

module.exports = {
    async findAll(req, res) {
        const unicorn = await connection('unicorn').select('*');

        return res.json(unicorn);
    },

    async store(req, res) {
        const {type, description} = req.body;
        const user_id = req.headers.authorization;

        const [id] = await connection('unicorn').insert({
            type,
            description,
            user_id,
        });

        return res.json({id});
    },

    async delete(req, res) {
        const {id} = req.params;
        const user_id = req.headers.authorization;

        const unicorn = await connection('unicorn')
            .where('id', id)
            .select('user_id')
            .first();

        if (unicorn.user_id !== user_id) {
            return res.status(401).json({error: 'Operation not permitted'});
        }

        await connection('unicorn').where('id', id).delete();

        return res.status(204).send();
    }
}
