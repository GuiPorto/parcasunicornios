const crypto = require('crypto');
const connection = require('../database/connection');

module.exports = {
    async findAll(req, res) {
        const user = await connection('user').select('*');

        return res.json(user);
    },

    async findOne(req, res) {
        const user_id = req.headers.authorization;

        const user = await connection('user')
            .where('id', user_id)
            .select('*');

        if (!user) {
            return res.status(400).json({error: 'User not found'});
        }

        return res.json(user);
    },

    async store(req, res) {
        const {name, date, document_RG, document_CPF, nickname, email} = req.body;

        const id = crypto.randomBytes(4).toString('HEX');
        const password = crypto.randomBytes(6).toString('HEX');

        await connection('user').insert({
            id,
            name,
            date,
            document_RG,
            document_CPF,
            nickname,
            email,
            password,
        })

        return res.json({id, password});
    }
}
